---
output: md_document
---

<!-- README.md is generated from README.Rmd. Please edit that file -->

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>",
  fig.path = "man/figures/README-",
  out.width = "100%"
)
```

# devCute

<!-- badges: start -->
<!-- badges: end -->

The package devCute is created for object analysis, it is also used for importing, exporting and printing result.

## Installation

You can install the development version of devCute like so:

```{r example}
library(devCute)
## basic example code
```


